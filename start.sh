#!/bin/bash
sudo apt-get update
echo y | sudo apt-get upgrade

# Installation of Terraform and Jenkins
echo y | sudo apt-get install sudo unzip vim
sudo wget -P /opt  http://mirrors.jenkins.io/war-stable/latest/jenkins.war
sudo chown stage:stage /opt/jenkins.war

# Generation of public ssh-key for user "stage"
#cd /home/stage
sudo ssh-keygen -N "" -f /root/.ssh/id_rsa
sudo cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys
#sudo chown stage:stage /home/stage/id_rsa /home/stage/id_rsa.pub
#sudo systemctl enable jenkins.service


touch jenkins.service
echo "[Unit]" >>jenkins.service
echo "Description=Jenkins Daemon" >>jenkins.service
echo >>jenkins.service
echo "[Service]" >>jenkins.service
echo "ExecStart=/usr/lib/jvm/java-8-oracle/jre/bin/java -jar /home/jenkins_user/jenkins.war --httpPort=8085" >>jenkins.service
echo "User=jenkins_user" >>jenkins.service
echo >>jenkins.service
echo "[Install]" >>jenkins.service
echo "WantedBy=multi-user.target" >>jenkins.service