# Installation of Ansible
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
echo y | sudo apt-get install ansible

# Modify file hosts in /etc/ansible/

# Installation of Oracle JDK version 8
sudo add-apt-repository ppa:webupd8team/java
sudo apt update
echo y | sudo apt install oracle-java8-installer
#sudo update-alternatives --config java
#sudo apt-get install oracle-java8-set-default
sudo vim /etc/environment
sudo echo "JAVA_HOME='/usr/lib/jvm/java-8-oracle/jre/bin/java'" >>/etc/environment
source /etc/environment
echo $JAVA_HOME

# Start of Jenkins
java -jar jenkins.war --httpPort=8085 &

# ssh-copy-id -i stage@10.1.1.11

# nur im Root account möglich
echo "cloudvm1.eastus.cloudapp.azure.com" >>/etc/ansible/hosts
echo "cloudvm5.eastus.cloudapp.azure.com" >>/etc/ansible/hosts
echo 10.1.1.11 >>/etc/ansible/hosts
echo 10.1.1.12 >>/etc/ansible/hosts
echo 10.1.1.13 >>/etc/ansible/hosts

VM1: cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys 
VM2: sudo cp /home/stage/.ssh/authorized_keys /root/.ssh/
VM3: sudo cp /home/stage/.ssh/authorized_keys /root/.ssh/
VM4: sudo cp /home/stage/.ssh/authorized_keys /root/.ssh/
VM5: sudo cp /home/stage/.ssh/authorized_keys /root/.ssh/

ansible-playbook --private-key=/root/.ssh/authorized_keys target1.yml

gitlab-ctl status
